export default {
  HOME : 'Home',
  NOVO_PEDIDO : 'NovoPedido',
  STACK : 
    {
      Home: { screen: '/src/views/Home/index.js' },
      NovoPedido: { screen: '/src/views/NovoPedido/index.js' }
    }  
}