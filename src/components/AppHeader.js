import React from 'react';
import { Header, Title, Button, Left, Right, Body, Icon} from 'native-base';

export default class AppHeader extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>PDVFácil</Title>
          </Body>
          <Right />
        </Header>        
    );
  }
}
