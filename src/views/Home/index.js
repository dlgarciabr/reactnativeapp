import React, { Component } from "react";

import { DrawerNavigator } from "react-navigation";
import SideBar from "../../components/SideBar";
import HomeScreen from "./HomeScreen";
import NovoPedido from "../NovoPedido/index.js";

const HomeScreenRouter = DrawerNavigator(
  {
    Home: { screen: HomeScreen },
    NovoPedido: { screen: NovoPedido }
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);
export default HomeScreenRouter ;