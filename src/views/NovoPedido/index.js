import React, { Component } from "react";

import SideBar from '../../components/SideBar';
import { DrawerNavigator } from "react-navigation";
import HomeScreen from "../Home/HomeScreen";
import NovoPedidoScreen from "./NovoPedidoScreen.js";

const NovoPedidoRouter = DrawerNavigator(
  {
    Home: { screen: HomeScreen },
    NovoPedido: { screen: NovoPedidoScreen }
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);
export default NovoPedidoRouter ;